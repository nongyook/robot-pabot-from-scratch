# robot-pabot-scratch

Git robot repo for pabot workshop

## Set-up venv
1. Use following command to initialize virtual environment
````shell
python3 -m venv venv
````
2. for activate venv
```shell
source venv/bin/activate
```


## Set-up project
Use following command to initialize project environment
````shell
pip install -r requirements.txt
````

## Run project
Use following command to run a test case
````shell
robot -d output testcase1.robot
````  

## Example command

[starter.sh](starter.sh) common robot framework command

[starter_pabot.sh](starter_pabot.sh) pabot instead common robot framework argument

[starter_pabot_order_tc.sh](starter_pabot_order_tc.sh) pabot with order testcase level

[starter_pabot_order.sh](starter_pabot_order.sh) pabot with order testcase level

[starter_pabot_tc_level.sh](starter_pabot_tc_level.sh) pabot with test case level

[starter_pabot_pabot_lib.sh](starter_pabot_pabot_lib.sh) pabot with test case level and demo shared suite setup


