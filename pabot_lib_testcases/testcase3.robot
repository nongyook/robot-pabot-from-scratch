*** Settings ***
Library    pabot.PabotLib

Suite Setup     Run Setup Only Once    
                ...     Run keywords    Print hello
                ...     AND             Print world

*** Keywords ***
Print ${data}
    Log    ${data}    console=True


*** Test Cases ***
TC3_1
    Log    no sleep

TC3_2
    sleep    2s
    Log    sleep 2s

TC3_3
    sleep    4s
    Log    sleep 4s